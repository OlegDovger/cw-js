Array.prototype.square = function () {
    var rslt = [];
    for (var i = 0; i < this.length; i++) {
        rslt.push(this[i] * this[i]);
    }
    return rslt;
};

Array.prototype.cube = function () {
    var rslt = [];
    for (var i = 0; i < this.length; i++) {
        rslt.push(this[i] * this[i] * this[i]);
    }
    return rslt;
};

Array.prototype.sum = function () {
    var rslt = 0;
    for (var i = 0; i < this.length; i++) {
        rslt += this[i];
    }
    return rslt;
};

Array.prototype.average = function () {
    return this.sum() / this.length;
};

Array.prototype.odd = function () {
    var rslt = [];
    for (var i = 0; i < this.length; i++) {
        if (this[i] % 2 == 1) {
            rslt.push(this[i]);
        }

    }
    return rslt;
};
Array.prototype.even = function () {
    var rslt = [];
    for (var i = 0; i < this.length; i++) {
        if (this[i] % 2 == 0) {
            rslt.push(this[i]);
        }

    }
    return rslt;
};
