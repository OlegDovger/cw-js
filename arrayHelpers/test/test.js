describe("arrayHelpers", function () {

    it("square", function () {
        expect([1,2,3,4,5].square()).toEqual([1,4,9,16,25]);
    });
    it("cube", function () {
        expect([1,2,3,4,5].cube()).toEqual([1,8,27,64,125]);
    });
    it("average", function () {
        expect([1,2,3,4,5].average()).toEqual(3);
    });
    it("odd", function () {
        expect([1,2,3,4,5].odd()).toEqual([1,3,5]);
    });
    it("even", function () {
        expect([1,2,3,4,5].even()).toEqual([2,4]);
    });
});
