describe("wishlist", function () {

    var wishlist = [
        {name: "Mini Puzzle", size: "small", clatters: "yes", weight: "light"},
        {name: "Toy Car", size: "medium", clatters: "a bit", weight: "medium"},
        {name: "Card Game", size: "small", clatters: "no", weight: "light"}
    ];

    var presents = [
        {size: "medium", clatters: "a bit", weight: "medium"},
        {size: "small", clatters: "yes", weight: "light"}
    ];

    it("find all objects", function () {
        expect(guessGifts(wishlist, presents)).toEqual(["Toy Car", "Mini Puzzle"]);
    });
});
