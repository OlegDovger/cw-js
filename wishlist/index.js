function guessGifts(wishlist, presents) {
    var rslt = [];
    var checkedMap = {};
    var isFound = true;
    for (var j = 0; j < presents.length; j++) {
        isFound = true;
        var present = presents[j];
        for (var i = 0; i < wishlist.length; i++) {
            isFound = true;
            var wish = wishlist[i];
            log(wish.name);

            for (var prop in wish) {
                if (present.hasOwnProperty(prop)) {
                    log(wish[prop] + " - " + present[prop]);
                    if (prop == "name") {
                        continue;
                    }
                    if (wish[prop] != present[prop]) {
                        isFound = false;
                        break;
                    } else {
                        continue;
                    }
                }
            }
            if (isFound) {
                if(!checkedMap.hasOwnProperty(wish.name)) {
                    rslt.push(wish.name);
                    checkedMap[wish.name] = true;
                }
            }
        }
    }

    return rslt;
}
