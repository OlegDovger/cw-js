function convertToRoman(number) {
    var convertedNum = "";
    var rDigitArr = [1000, 500, 100, 50, 10, 5, 1];
    var rScale = "MDCLXVI";
    var regTest = /(I{4}|V{1}I{4}|X{4}|L{1}X{4}|C{4}|D{1}C{4})/g;

    function checkNumber(number) {
        if (regTest.test(convertedNum)) {
            var groupToConvert = convertedNum.match(regTest)[0];
            var bigDigit = rScale[rScale.indexOf(groupToConvert[0]) - 1] || rScale[0];
            var smallDigit = groupToConvert[1];

            convertedNum = convertedNum.replace(groupToConvert, smallDigit + bigDigit);
        }
        for (var i = 0; i < rDigitArr.length; i++) {
            var rDigit = rDigitArr[i];
            if (number >= rDigit) {
                convertedNum += rScale[i];
                checkNumber(number - rDigit);
                break;
            }
        }
    }

    checkNumber(number);
    return convertedNum;
}