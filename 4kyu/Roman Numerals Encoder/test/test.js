describe("tmp", function () {
    it("test", function () {
        expect(convertToRoman(1)).toEqual("I");
        expect(convertToRoman(2)).toEqual("II");
        expect(convertToRoman(3)).toEqual("III");

        expect(convertToRoman(4)).toEqual("IV");

        expect(convertToRoman(5)).toEqual("V");
        expect(convertToRoman(6)).toEqual("VI");
        expect(convertToRoman(7)).toEqual("VII");
        expect(convertToRoman(8)).toEqual("VIII");
        expect(convertToRoman(9)).toEqual("IX");
        expect(convertToRoman(2014)).toEqual("MMXIV");
    });
});