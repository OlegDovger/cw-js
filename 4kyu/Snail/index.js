Array.prototype.fill = function (times) {
    var a = [];
    for (var i = 0; i < times; i += 1) {
        a.push(i);

    }
    return a;
};
var snail = function (array) {
    var arr = [];
    if(!array || array.length == 0) {
        return arr;
    }
    var arrayX = [].fill(array[0].length), arrayY = [].fill(array.length);

    var aX = arrayX[0], aY = arrayY[0];

    function moveUp (arrX, arrY, arr) {
        var arrayUp = arrX || arrY;
        for (var i = 0; i < arrayUp.length; i++) {
            aY = (arrY && arrY[i] !=null)? arrY[i] : aY;
            aX = (arrX && arrX[i] !=null)? arrX[i] : aX;
            arr.push(array[aY][aX]);
        }
        return arrayUp[arrayUp.length - 1];
    }
    function moveDown (arrX, arrY, arr) {
        var arrayDown = arrX || arrY;
        for (var i = arrayDown.length - 1; i > -1; i--) {
            aY = (arrY && arrY[i] !=null)? arrY[i] : aY;
            aX = (arrX && arrX[i] !=null)? arrX[i] : aX;
            arr.push(array[aY][aX]);
        }
        return arrayDown[0];
    }
    while (arrayX.length > 0 || arrayY.length > 0) {
        // y - stop, x - move up
        aX = moveUp(arrayX, null, arr);
        arrayY.splice(0, 1);

        // y - move up, x - stop
        aY = moveUp(null, arrayY, arr);
        arrayX.splice(arrayX.length - 1, 1);

        // y - stop, x - move down
        aX = moveDown(arrayX, null, arr);
        arrayY.splice(arrayY.length - 1, 1);

        // y - move down, x - stop
        aY = moveDown(null, arrayY, arr);
        arrayX.splice(0, 1);

    }
    return arr;
};