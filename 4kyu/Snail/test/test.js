describe("Snail", function () {
    it("test", function () {
        expect(snail()).toEqual([]);
        expect(snail([])).toEqual([]);
        expect(snail([[1]])).toEqual([1]);
        expect(snail([
            [1,2],
            [1,2]
        ])).toEqual([1,2,2,1]);
        expect(snail([
            [1,2,3],
            [1,2,3],
            [1,2,3]
        ])).toEqual([1,2,3,3,3,2,1,1,2]);
    });
});