/**
 * @return {number}
 */
function Increment () {
    var i = 0;
    this.toString = function () {
        return ++i;
    }
}