describe("incrementObject", function () {

    it("test", function () {
        var increment = new Increment();
        expect(1).toBe(+increment);
        expect(2).toBe(+increment);
        expect(7).toBe(increment+increment);
    });
});