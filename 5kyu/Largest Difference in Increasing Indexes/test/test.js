describe("Largest Difference in Increasing Indexes", function () {

    it("test", function () {
        expect(largestDifference()).toBe(0);
        expect(largestDifference([])).toBe(0);
        expect(largestDifference([1, 1])).toBe(1);
        expect(largestDifference([1, 2])).toBe(1);
        expect(largestDifference([1, 1, 2])).toBe(2);
        expect(largestDifference([1, 2, 3])).toBe(2);
        expect(largestDifference([2, 1, 3])).toBe(1);
        expect(largestDifference([2, -1, 3])).toBe(1);
        expect(largestDifference([100, 4, 1, 10, 3, 4, 0, -1, -7, -6, -10])).toBe(5);
    });
});
