Write a function which takes an array data of numbers and returns the largest difference in indexes j - i such that data[i] <= data[j]

Example:
```
largestDifference([1,2,3]) ; // returns 2
```
This kata is sometimes used as an interview question. Like a lot of interview questions, there are a number of solutions.

The goal of this kata is to make your solution as computationally efficient as possible. The best solution runs in linear time in the input