var largestDifference = function(data) {
    data = data instanceof Array ? data : [];
    var m = data[0];
    var min = m, max = m;
    var min_i = 0, max_i = 0;
    for (var i = 0, l = data.length; i < l; i++) {

        if(data[i] < min) {
            min = data[i];
            min_i = i;
        }
        if(data[i] >= max) {
            max = data[i];
            max_i = i;
        }
    }
    return max_i - min_i;
};