function meters(x) {
    // todo: return value scaled to correct unit
    var num = 0;
    var prefix = "";
    var measure = "m";

    var k = 1000, M = 1000 * k, G = 1000 * M, T = 1000 * G;
    var P = 1000 * T, E = 1000 * P, Z = 1000 * E, Y = 1000 * Z;

    if(x > Y) {
        num = x/Y, prefix = "Y";
    } else if (x >= Z) {
        num = x/Z, prefix = "Z";
    } else if (x >= E) {
        num = x/E, prefix = "E";
    } else if (x >= P) {
        num = x/P, prefix = "P";
    } else if (x >= T) {
        num = x/T, prefix = "T";
    } else if (x >= G) {
        num = x/G, prefix = "G";
    } else if (x >= M) {
        num = x/M, prefix = "M";
    } else if (x >= k) {
        num = x/k, prefix = "k";
    } else {
        num = x || num;
    }
    return num + prefix + measure;
}
