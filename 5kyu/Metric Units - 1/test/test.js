describe("Metric Units - 1", function () {

    it("test", function () {
        expect(meters()).toBe("0m");
        expect(meters(3)).toBe("3m");
        expect(meters(300)).toBe("300m");
        expect(meters(3000)).toBe("3km");
    });
});

