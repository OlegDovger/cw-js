function RNG(n) {
    this.n = n;
}

RNG.prototype.rand = function() {
    if(this.arr === undefined || this.arr.length == 0) {
        this.arr = [];
        for (var i = 0; i < this.n; i++) {
            this.arr.push(i);
        }
    }
    if(this.arr.length > 0) {

        var pos = Math.floor(Math.random()*this.arr.length);

        var number = parseInt(this.arr[pos]);
        this.arr.splice(pos, 1);
    }

    return number;
};