function pigIt(str) {
    var rslt = "";

    var words = str.split(" ");
    for (var i = 0; i < words.length; i++) {
        var word = words[i];
        rslt = rslt + word.substring(1) + word.charAt(0) + "ay" + (i < words.length - 1 ? " ": "");
    }
    return rslt;
}
