function matrixMultiplication(a, b) {
    var array = [];
    var aWidth = a.length;
    for (var i = 0; i < aWidth; i++) {
        for (var k = 0; k < aWidth; k++) {
            var num = 0;
            for (var j = 0; j < aWidth; j++) {
                var elA = a[i][j];
                var elB = b[j][k];
                num += elA * elB;
            }
            array[i] = array[i] || [];
            array[i][k] = array[i][k] || [];
            array[i][k] = num;
        }
    }
    return array;
}
