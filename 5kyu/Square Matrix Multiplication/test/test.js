describe("Square Matrix Multiplication", function () {
    it("test", function () {
        expect(matrixMultiplication([], [])).toEqual([]);
        expect(matrixMultiplication([
            [1, 2],
            [1, 2]
        ], [
            [1, 2],
            [1, 2]
        ])).toEqual([
            [3, 6],
            [3, 6]
        ]);
        expect(matrixMultiplication([
            [1, 2],
            [3, 4]
        ], [
            [5, 6],
            [7, 8]
        ])).toEqual([
            [19, 22],
            [43, 50]
        ]);
    });
});
