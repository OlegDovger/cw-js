// TODO: complete this object/class

// The constructor takes in an array of items and a integer indicating how many
// items fit within a single page
function PaginationHelper(collection, itemsPerPage) {
    this.collection = (collection instanceof Array) ? collection : [];
    this.itemsPerPage = itemsPerPage;
}

// returns the number of items within the entire collection
PaginationHelper.prototype.itemCount = function () {
    return this.collection.length;
};

// returns the number of pages
PaginationHelper.prototype.pageCount = function () {
    if(this.collection.length == 0) {
        return 0;
    }
    return Math.round(this.collection.length / this.itemsPerPage);
};

// returns the number of items on the current page. page_index is zero based.
// this method should return -1 for pageIndex values that are out of range
PaginationHelper.prototype.pageItemCount = function (pageIndex) {
    var iPrePage = this.itemsPerPage;
    var number = iPrePage * (pageIndex + 1);
    var collection = this.collection;
    if(number > collection.length) {
        var diff = Math.abs(number - collection.length);
        if(diff < iPrePage) {
            return iPrePage - diff;
        }
        return -1;
    } else {
        return iPrePage;
    }
};

// determines what page an item is on. Zero based indexes
// this method should return -1 for itemIndex values that are out of range
PaginationHelper.prototype.pageIndex = function (itemIndex) {
    var rslt = -1;
    itemIndex = itemIndex + 1;
    if(this.collection.length >= itemIndex) {
        if( itemIndex <= this.itemsPerPage) {
            return 0;
        } else {
            return Math.round(itemIndex / this.itemsPerPage) - 1;
        }
    }
    return rslt;
};
