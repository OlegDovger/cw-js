describe("PaginationHelper", function () {

    it("pageCount", function () {
        var pHelper = new PaginationHelper(null, 4);

        expect(pHelper.pageCount()).toEqual(0);

        var pHelper = new PaginationHelper(["a","b","c","d","e"], 5);

        expect(pHelper.pageCount()).toEqual(1);

        var pHelper = new PaginationHelper(["a","b","c","d","e"], 2);

        expect(pHelper.pageCount()).toEqual(3);
        expect(pHelper.itemCount()).toEqual(5);

        expect(pHelper.pageItemCount(0)).toEqual(2);
        expect(pHelper.pageItemCount(1)).toEqual(2);
        expect(pHelper.pageItemCount(2)).toEqual(1);
        expect(pHelper.pageItemCount(56)).toEqual(-1);

        expect(pHelper.pageIndex(0)).toEqual(0);
        expect(pHelper.pageIndex(2)).toEqual(1);
        expect(pHelper.pageIndex(3)).toEqual(1);
        expect(pHelper.pageIndex(5)).toEqual(-1);

        expect(pHelper.pageIndex(56)).toEqual(-1);


        var pHelper = new PaginationHelper(['a','b','c','d','e','f'], 4);

        expect(pHelper.pageCount()).toEqual(2);
        expect(pHelper.itemCount()).toEqual(6);

        expect(pHelper.pageItemCount(0)).toEqual(4);
        expect(pHelper.pageItemCount(1)).toEqual(2);
        expect(pHelper.pageItemCount(2)).toEqual(-1);
        expect(pHelper.pageItemCount(56)).toEqual(-1);

        expect(pHelper.pageIndex(0)).toEqual(0);
        expect(pHelper.pageIndex(2)).toEqual(0);
        expect(pHelper.pageIndex(3)).toEqual(0);
        expect(pHelper.pageIndex(5)).toEqual(1);

        expect(pHelper.pageIndex(56)).toEqual(-1);
    });
});