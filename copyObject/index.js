function clone(obj) {

    if (null == obj || "object" != typeof obj) return obj;

    var copy;
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
    }

    if (obj instanceof Array) {
        copy = [];
        for (var i = 0; i < obj.length; i++) {
            copy[i] = clone(obj[i]);
        }
    }

    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) {
                copy[attr] = clone(obj[attr]);
            }
        }
    }

    return copy;

    throw new Error("Type is not valid");
}