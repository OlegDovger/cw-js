describe("copyObject: copy object", function () {

    var obj = {
        one: 'true',
        two: 2,
        three: {
            one: 'false',
            two: 2,
            three: [ 'lol', 3, true, undefined]
        }
    };
    var objCloned = clone(obj);
    var objCloned2 = clone(obj);


    it("must duplicate object", function () {
        expect(obj).not.toBe(objCloned);
        expect(obj.three).not.toBe(objCloned.three);
        expect(obj.three.three).not.toBe(objCloned.three.three);
    });
});